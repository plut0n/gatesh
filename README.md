# Gatesh
## Overview

Gatesh - Download (Tor option) a list of all vpngate.net vpns, display their characteristics, and connect to selected one using openvpn

## Installation

### Dependencies

Requirements: openvpn wget tor (not mandatory)

Debian:
```
~$ sudo apt-get update && sudo apt-get install openvpn wget tor
```

ArchLinux:
```
~$ sudo pacman -S openvpn wget tor
```

### Git and Installation

Debian & ArchLinux

```
~$ git clone https://gitlab.com/plut0n/gatesh.git
~$ chmod +x gatesh/gate.sh
~$ cp gatesh/gate.sh /bin/gatesh
```

## Usage

Launching gatesh without Tor
```
~$ gatesh
```

Tor option (launch Tor service, if not already started, in order to download the vpn list)
Beware, this script doesn't stop Tor service!

```
~$ gatesh -t
```

## More information

This script download the vpn list and store it in ```/tmp/gate```
This script is under the BSD clause2 license (cf. LICENSE)