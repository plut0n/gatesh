#!/bin/bash

function Help {
    cat << EOF
    usage : gatesh [-t] [-h]
    -h      display this $(echo -e "\e[4mHelp\e[0m") message
    -t      download vpngate's vpn list through $(echo -e "\e[4mTor\e[0m")
EOF
}

function TorGet {
    ps auxw | grep /usr/bin/tor | grep -v grep > /dev/null
    if [ $? != 0 ]
    then
        startor=1
        echo -e "Starting Tor..."
        sudo systemctl start tor
    fi
    sleep 3.
    ok=1
    while [ $ok != 0 ]
    do
        sudo systemctl status tor | grep -q Done
        ok = $?
    done
    echo -e "Getting Vpngate's vpns list & info...\n"
    curl http://www.vpngate.net/en/ > /tmp/gate/vpngate.html
}

function Display {
    echo -e "\nn: Country IP Active_Sessions Troughput speed Ping"
    for i in `seq 1 $(cat /tmp/gate/vpn | wc -l)`; do
        echo -ne "$i: "
        grep -oh "flags/\w*" /tmp/gate/vpn | cut -c7- | sed -n $i\p | tr -d "\n" && echo -ne " "
        grep -oh "10pt;'>\w*.\w*.\w*.\w*" /tmp/gate/vpn | cut -c8- | sed -n $((1+($i-1)*3))\p | tr -d "\n" && echo -ne " "
        grep -oh "10pt;'>\w*" /tmp/gate/vpn | cut -c8- | sed -n $((2+($i-1)*3))\p | tr -d "\n"&& echo -ne "  "
        grep -oh "10pt;'>\w*.\w*.\w*" /tmp/gate/vpn | cut -c8- | sed -n $((3+($i-1)*3))\p | tr -d "\n" && echo -ne " "
        grep -oh "<b>\w* ms" /tmp/gate/vpn | cut -c4- | sed -n $i\p | tr -d "\n" && echo -e " "
    done
}

function SelectVPN {
    echo -ne "\nVPN number: "
    read nvpn
    url=$(grep -o "do_openvpn.aspx?fqdn=\w*.opengw.net&ip=\w*.\w*.\w*.\w*&tcp=\w*&udp=\w*&sid=\w*" /tmp/gate/vpn | sed -n $nvpn\p)
    curl "www.vpngate.net/en/$url" > /tmp/gate/info.html
    ovpn=$(grep common /tmp/gate/info.html | grep udp | awk '{print $2}' | cut -c7- | sed 's/">//' | sed -n 1p | tr -d "\n")
    echo -e "Downloading selected vpn openvpn config file..."
    wget --quiet -O /tmp/gate/vpn.ovpn "www.vpngate.net$ovpn"
    sed -i "s/remote  0/remote $(grep "File: " /tmp/gate/info.html | awk '{print $4}' | rev | cut -c11- | rev | sed -n 1p) $(grep "File: " /tmp/gate/info.html | awk '{print $5}' | rev | cut -c14- | rev | sed -n 1p)/g" /tmp/gate/vpn.ovpn
}

function ConnectVPN {
    echo -e "Connecting to the selected vpn..."
    sudo openvpn /tmp/gate/vpn.ovpn > /tmp/gate/vpn.log &
    echo -e "Press q to quit connection attempt"
    while true; do
        read -n 1 input
        if grep -q "Initialization Sequence Completed" /tmp/gate/vpn.log
        then
            echo -e "/nSuccessfull connection!"
            connected=1
            break 2
        fi
        if [[ $input == "q" ]] || [[ $input == "Q" ]]
        then
            echo -ne "Connection Reset!\nTry another vpn [Y/n]: "
            read again
            if [[ "$again" =~ ^([nN][oO]|[nN])$ ]]; then
                connected=0
                exit 0
            fi
            break
        fi
    done
    echo "connected $connected"
}

function Exit {
    if [ $tor == 1 ]; then
        if [  $ok != 0 ] || [  $startor == 1 ]; then
            sudo systemctl stop tor
        fi
    fi
    if [ $connected != 1 ]; then
        sudo pkill openvpn
        echo -e "\nOh sh*t, you're naked on the net!"
    fi
    sudo rm -r /tmp/gate
}

function Intro {
    echo -e "                                                      "
    echo -e "  ██████╗  █████╗ ████████╗███████╗   ███████╗██╗  ██╗"
    echo -e " ██╔════╝ ██╔══██╗╚══██╔══╝██╔════╝   ██╔════╝██║  ██║"
    echo -e " ██║  ███╗███████║   ██║   █████╗     ███████╗███████║"
    echo -e " ██║   ██║██╔══██║   ██║   ██╔══╝     ╚════██║██╔══██║"
    echo -e " ╚██████╔╝██║  ██║   ██║   ███████╗██╗███████║██║  ██║"
    echo -e "  ╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚══════╝╚═╝╚══════╝╚═╝  ╚═╝"
}

connected=0
tor=0
Intro
if [ ! -d /tmp/gate ]; then
    mkdir /tmp/gate &> /dev/null
else
    echo -e "Folder /tmp/gate already exist, quitting to avoid side effect"
    exit 1
fi
trap "Exit" EXIT
while getopts "h\?t" opt; do
    case $opt in
        h|\?)
            Help
            exit 1
            ;;
        t)
            tor=1
            shift
            ;;
    esac
done

sudo pkill openvpn
if [ $tor == 1 ]; then
    TorGet
else
    echo -e "Getting vpngate's vpns list & info...\n"
    curl http://www.vpngate.net/en/ > /tmp/gate/vpngate.html
fi
grep "opengw.net" /tmp/gate/vpngate.html | grep "openvpn.aspx" > /tmp/gate/vpn
column -t <<< $(Display)
while [ $connected != 1 ]; do
    tput sc
    SelectVPN
    ConnectVPN
    tput rc
    tput ed
done
